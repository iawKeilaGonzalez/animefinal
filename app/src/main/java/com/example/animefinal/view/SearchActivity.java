package com.example.animefinal.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.animefinal.R;
import com.example.animefinal.controller.Controller;

import org.checkerframework.common.subtyping.qual.Bottom;

public class SearchActivity extends AppCompatActivity implements ViewActivity{
    View home, profile, search, logout;
    RecyclerView recyclerView;
    EditText input;
    Button searchBtn, searchUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        callControllerWithThisActivityAsParameter();
    }

    @Override
    public void callControllerWithThisActivityAsParameter() {
        Controller.getInstance().searchActivity(this);
    }

    @Override
    public void createAllItemsAsGlobalWithGetters() {
        home = findViewById(R.id.home);
        profile = findViewById(R.id.profile);
        search = findViewById(R.id.search);
        logout = findViewById(R.id.logout);
        recyclerView = findViewById(R.id.recyclerViewSearch);
        input = findViewById(R.id.input_search);
        searchBtn = findViewById(R.id.searchBtn);
        searchUser = findViewById(R.id.searchUser);
    }


    public View getHome() {return home;}
    public View getProfile() {return profile;}
    public View getSearch() {return search;}
    public View getLogout() {return logout;}
    public RecyclerView getRecyclerView() {return recyclerView;}
    public EditText getInput() {return input;}
    public Button getSearchBtn() {return searchBtn;}
    public Button getSearchUser() {return searchUser;}
}