package com.example.animefinal.view;

public interface ViewActivity {
    void callControllerWithThisActivityAsParameter();
    void createAllItemsAsGlobalWithGetters();
}
