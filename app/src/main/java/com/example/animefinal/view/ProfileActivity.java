package com.example.animefinal.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.animefinal.R;
import com.example.animefinal.controller.Controller;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class ProfileActivity extends AppCompatActivity implements ViewActivity {
    private TextView name, username;
    View home, profile, search, logout;
    RecyclerView recyclerViewFavs, recyclerViewFriends;
    Button follow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        callControllerWithThisActivityAsParameter();
    }

    @Override
    public void callControllerWithThisActivityAsParameter() {
        Controller.getInstance().profileActivity(this);
    }

    @Override
    public void createAllItemsAsGlobalWithGetters() {
        this.name = findViewById(R.id.name);
        this.username = findViewById(R.id.username);
        home = findViewById(R.id.home);
        profile = findViewById(R.id.profile);
        search = findViewById(R.id.search);
        logout = findViewById(R.id.logout);
        recyclerViewFavs = findViewById(R.id.recyclerViewFavs);
        recyclerViewFriends = findViewById(R.id.recyclerViewFriends);
        follow = findViewById(R.id.button);
    }


    public TextView getName() {
        return name;
    }
    public TextView getUsername() {
        return username;
    }
    public View getHome() {return home;}
    public View getProfile() {return profile;}
    public View getSearch() {return search;}
    public View getLogout() {return logout;}
    public RecyclerView getRecyclerViewFavs() {return recyclerViewFavs;}
    public RecyclerView getRecyclerViewFriends() {return recyclerViewFriends;}
    public Button getFollow() {return follow;}

}