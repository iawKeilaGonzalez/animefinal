package com.example.animefinal.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.animefinal.R;
import com.example.animefinal.controller.Controller;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class LogActivity extends AppCompatActivity implements ViewActivity {
    TextView signupRedirect;
    Button login_button;
    EditText email, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (getIntent().getExtras() != null){
            createAllItemsAsGlobalWithGetters();
        }

        callControllerWithThisActivityAsParameter();
    }

    public void callControllerWithThisActivityAsParameter(){
        Controller.getInstance().logActivity(this);
    };

    public void createAllItemsAsGlobalWithGetters(){
        signupRedirect = findViewById(R.id.signupRedirect);
        login_button = findViewById(R.id.login_button);
        email = findViewById(R.id.login_username);
        password = findViewById(R.id.login_password);
    }

    public Button getLoginBtn() {return login_button;}
    public TextView getSignupRed() {
        return signupRedirect;
    }
    public EditText getEmail() {
        return email;
    }
    public EditText getPassword() {
        return password;
    }

}