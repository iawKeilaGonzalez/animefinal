package com.example.animefinal.view;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import com.example.animefinal.R;
import com.example.animefinal.controller.ControllerInterface;


public class SplashActivity extends AppCompatActivity implements ControllerInterface {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_main);

        doSplashIntent();
    }

    private void doSplashIntent() {
        if (readDataWithSharePrefence(this) == null) {
            Intent intent = new Intent(SplashActivity.this, LogActivity.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(SplashActivity.this, MenuActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void createActivityButtons() {
    }

    @Override
    public String readDataWithSharePrefence(Activity activity) {
        return ControllerInterface.super.readDataWithSharePrefence(activity);
    }
}