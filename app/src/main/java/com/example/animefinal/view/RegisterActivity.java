package com.example.animefinal.view;



import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.animefinal.R;
import com.example.animefinal.controller.Controller;

public class RegisterActivity extends AppCompatActivity implements ViewActivity {
    private Button registerBtn;
    private TextView loginBtn;
    private EditText name,email,username,password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        if (getIntent().getExtras() != null){
            createAllItemsAsGlobalWithGetters();
        }

        callControllerWithThisActivityAsParameter();
    }

    public void callControllerWithThisActivityAsParameter(){
        Controller.getInstance().registerActivity(this);
    };

    public void createAllItemsAsGlobalWithGetters(){
        this.registerBtn = (Button) findViewById(R.id.signup_button);
        this.loginBtn = (TextView) findViewById(R.id.loginRedirect);
        this.name = (EditText) findViewById(R.id.signup_name);
        this.email = (EditText) findViewById(R.id.signup_email);
        this.username = (EditText) findViewById(R.id.signup_username);
        this.password = (EditText) findViewById(R.id.signup_password);
    }

    public Button getBtn() {
        return registerBtn;
    }
    public TextView getLoginRed() {
        return loginBtn;
    }
    public EditText getName() {
        return name;
    }
    public EditText getEmail() {
        return email;
    }
    public EditText getUsername() {
        return username;
    }
    public EditText getPassword() {
        return password;
    }
}