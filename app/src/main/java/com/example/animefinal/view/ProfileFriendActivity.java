package com.example.animefinal.view;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.animefinal.R;
import com.example.animefinal.controller.Controller;

public class ProfileFriendActivity extends AppCompatActivity implements ViewActivity {
    private TextView name, username;
    View home, profile, search, logout;
    RecyclerView recyclerViewFavsFriend;
    String nameStr, usernameStr, emailStr;
    Button follow;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_friend);

        callControllerWithThisActivityAsParameter();
    }

    @Override
    public void callControllerWithThisActivityAsParameter() {
        Controller.getInstance().profileFriendActivity(this);
    }

    @Override
    public void createAllItemsAsGlobalWithGetters() {
        this.name = findViewById(R.id.nameProfileFriend);
        this.username = findViewById(R.id.usernameProfileFriend);
        home = findViewById(R.id.home);
        profile = findViewById(R.id.profile);
        search = findViewById(R.id.search);
        logout = findViewById(R.id.logout);
        recyclerViewFavsFriend = findViewById(R.id.recyclerViewFavsFriend);
        nameStr = getIntent().getStringExtra("friendName");
        usernameStr = getIntent().getStringExtra("friendUsername");
        emailStr = getIntent().getStringExtra("friendEmail");
        follow = findViewById(R.id.button);
    }

    public TextView getName() {
        return name;
    }
    public TextView getUsername() {
        return username;
    }
    public View getHome() {return home;}
    public View getProfile() {return profile;}
    public View getSearch() {return search;}
    public View getLogout() {return logout;}
    public RecyclerView getRecyclerViewFavsFriend() {return recyclerViewFavsFriend;}
    public String getNameStr() {return nameStr;}
    public String getUsernameStr() {return usernameStr;}
    public String getEmailStr() {return emailStr;}
    public Button getFollow() {return follow;}

}