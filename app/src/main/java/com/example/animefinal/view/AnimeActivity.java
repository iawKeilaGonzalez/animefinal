package com.example.animefinal.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.animefinal.R;
import com.example.animefinal.controller.Controller;
import com.example.animefinal.model.Anime;

public class AnimeActivity extends AppCompatActivity implements ViewActivity{
    View home, profile, search, logout;
    TextView name,type,year,desc;
    ImageView img;
    String nameStr, typeStr, yearStr, descStr, imageStr;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anime);
        callControllerWithThisActivityAsParameter();
    }

    @Override
    public void callControllerWithThisActivityAsParameter() {
        Controller.getInstance().animeActivity(this);
    }

    @Override
    public void createAllItemsAsGlobalWithGetters() {
        home = findViewById(R.id.home);
        profile = findViewById(R.id.profile);
        search = findViewById(R.id.search);
        logout = findViewById(R.id.logout);
        name = findViewById(R.id.nameAnime);
        type = findViewById(R.id.typeAnime);
        year = findViewById(R.id.yearAnime);
        desc = findViewById(R.id.animeDesc);
        img = findViewById(R.id.imageAnime);
        nameStr = getIntent().getStringExtra("animeName");
        typeStr = getIntent().getStringExtra("animeType");
        descStr = getIntent().getStringExtra("animeDesc");
        yearStr = getIntent().getStringExtra("animeYear") + "";
        imageStr = getIntent().getStringExtra("animeImage");
    }

    public View getHome() {return home;}
    public View getProfile() {return profile;}
    public View getSearch() {return search;}
    public View getLogout() {return logout;}
    public TextView getName() {
        return name;
    }

    public TextView getType() {
        return type;
    }

    public TextView getYear() {
        return year;
    }

    public TextView getDesc() {
        return desc;
    }

    public ImageView getImg() {
        return img;
    }

    public String getNameStr() {
        return nameStr;
    }

    public String getTypeStr() {
        return typeStr;
    }

    public String getYearStr() {
        return yearStr;
    }

    public String getImageStr() {
        return imageStr;
    }

    public String getDescStr() {
        return descStr;
    }

}