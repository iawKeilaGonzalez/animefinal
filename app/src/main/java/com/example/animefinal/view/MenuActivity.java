package com.example.animefinal.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.animefinal.R;
import com.example.animefinal.controller.Controller;
import com.example.animefinal.model.Usuario;
import com.example.animefinal.utils.Constants;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.io.Serializable;

public class MenuActivity extends AppCompatActivity  implements ViewActivity{
    View home, profile, search, logout;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        callControllerWithThisActivityAsParameter();
    }

    @Override
    public void callControllerWithThisActivityAsParameter(){
        Controller.getInstance().menuActivity(this);
    };

    @Override
    public void createAllItemsAsGlobalWithGetters() {
        home = findViewById(R.id.home);
        profile = findViewById(R.id.profile);
        search = findViewById(R.id.search);
        logout = findViewById(R.id.logout);
        recyclerView = findViewById(R.id.recyclerViewAnimes);
    }

    public View getHome() {return home;}
    public View getProfile() {return profile;}
    public View getSearch() {return search;}
    public View getLogout() {return logout;}
    public RecyclerView getRecyclerView() {return recyclerView;}
}