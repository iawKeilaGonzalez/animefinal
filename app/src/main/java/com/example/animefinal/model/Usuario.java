

package com.example.animefinal.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Usuario implements Serializable {

    private String name;
    private String email;
    private String username;

    public Usuario(String name, String email, String username) {
        this.email = email;
        this.name = name;
        this.username = username;
    }

    public Usuario(String email, String username) {
        this.email = email;
        this.username = username;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
}
