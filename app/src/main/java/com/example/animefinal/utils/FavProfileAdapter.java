package com.example.animefinal.utils;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.animefinal.R;
import com.example.animefinal.dao.FirestoreDB;
import com.example.animefinal.model.Anime;
import com.example.animefinal.view.AnimeActivity;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;

public class FavProfileAdapter extends RecyclerView.Adapter<FavProfileAdapter.MyViewHolder> {

    private ArrayList<Anime> animes;
    private Context context;
    private FirestoreDB persistencia;
    private Boolean isFriend;

    public FavProfileAdapter(ArrayList<Anime> animes, Context context, FirestoreDB persistencia, boolean isFriend) {
        this.animes = animes;
        this.context = context;
        this.persistencia = persistencia;
        this.isFriend = isFriend;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.anime_row_profile, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.animeTitle.setText(animes.get(position).getName());
        holder.animeYear.setText(animes.get(position).getYear() + "");

        if (!isFriend){
            persistencia.getUser().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                @Override
                public void onSuccess(DocumentSnapshot documentSnapshot) {
                    boolean existe = false;
                    for (Map<String, Object> map : (ArrayList<Map<String, Object>>) documentSnapshot.get("fav")) {
                        if (map.get("name").equals(animes.get(holder.getAdapterPosition()).getName())){
                            existe = true;
                            break;
                        }
                    }
                    if (existe){
                        holder.favIcon.setImageResource(R.drawable.ic_baseline_favorite_24);
                    }else{
                        holder.favIcon.setImageResource(R.drawable.ic_baseline_favorite_border_24);
                    }
                }
            });

            holder.favIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(holder.favIcon.getDrawable().getConstantState() == context.getDrawable(R.drawable.ic_baseline_favorite_border_24).getConstantState()) {
                        holder.favIcon.setImageResource(R.drawable.ic_baseline_favorite_24);
                        persistencia.updateFavs(animes.get(holder.getAdapterPosition()),true);
                    } else {
                        holder.favIcon.setImageResource(R.drawable.ic_baseline_favorite_border_24);
                        persistencia.updateFavs(animes.get(holder.getAdapterPosition()),false);
                    }
                }
            });

            holder.relative.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, AnimeActivity.class);
                    int position = holder.getAdapterPosition();
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("animeName", animes.get(position).getName());
                    intent.putExtra("animeType", animes.get(position).getType());
                    intent.putExtra("animeDesc", animes.get(position).getDescription());
                    intent.putExtra("animeYear", animes.get(position).getYear());
                    intent.putExtra("animeImage", animes.get(position).getImage());
                    context.startActivity(intent);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return animes.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView animeTitle;
        private TextView animeYear;
        private ImageView favIcon;
        private RelativeLayout relative;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            animeTitle = itemView.findViewById(R.id.animeTitleProf);
            animeYear = itemView.findViewById(R.id.animeYearProf);
            favIcon = itemView.findViewById(R.id.imageViewProf);
            relative = itemView.findViewById(R.id.relativeAnimeRow);

        }
    }
}
