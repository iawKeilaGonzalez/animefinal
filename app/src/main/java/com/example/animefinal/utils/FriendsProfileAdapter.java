package com.example.animefinal.utils;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.animefinal.R;
import com.example.animefinal.dao.FirestoreDB;
import com.example.animefinal.model.Anime;
import com.example.animefinal.model.Usuario;
import com.example.animefinal.view.ProfileFriendActivity;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;

import java.util.ArrayList;
import java.util.Map;

public class FriendsProfileAdapter extends RecyclerView.Adapter<FriendsProfileAdapter.MyViewHolder> {

    private ArrayList<Usuario> friends;
    private Context context;
    private FirestoreDB persistencia;

    public FriendsProfileAdapter(ArrayList<Usuario> friends, Context context, FirestoreDB persistencia) {
        this.friends = friends;
        this.context = context;
        this.persistencia = persistencia;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.friends_row_profile, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.friendUsername.setText(friends.get(position).getUsername());
        holder.friendName.setText(friends.get(position).getName());
        holder.friendRelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProfileFriendActivity.class);
                int position = holder.getAdapterPosition();
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("friendName", friends.get(position).getName());
                intent.putExtra("friendUsername", friends.get(position).getUsername());
                intent.putExtra("friendEmail", friends.get(position).getEmail());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return friends.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView friendUsername;
        private TextView friendName;
        private RelativeLayout friendRelative;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            friendUsername = itemView.findViewById(R.id.friendUsernameprofile);
            friendName = itemView.findViewById(R.id.friendNameProfile);
            friendRelative = itemView.findViewById(R.id.friendRelative);
        }
    }
}
