package com.example.animefinal.dao;

import com.example.animefinal.controller.Controller;
import com.example.animefinal.model.Anime;
import com.example.animefinal.model.Usuario;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FirestoreDB {

    public void save() {
        Usuario user = Controller.usuario;
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        HashMap<String, Object> collection = new HashMap<String, Object>();

        if(user.getName() != null ) {
            collection.put("name", user.getName());
        }
        if(user.getEmail() != null ) {
            collection.put("email", user.getEmail());
        }
        if(user.getUsername() != null ) {
            collection.put("username", user.getUsername());
        }
        collection.put("fav", new ArrayList<>());
        collection.put("friends", new ArrayList<>());
        db.collection("users").document(user.getEmail()).set(collection, SetOptions.merge());
    }

    public Task<DocumentSnapshot> getUser() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        return db.collection("users").document(Controller.usuario.getEmail()).get();
    }

    public Task<DocumentSnapshot> getFriend(String email) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        return db.collection("users").document(email).get();
    }

    public void updateFavs(Anime anime, boolean isAdd) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference user = db.collection("users").document(Controller.usuario.getEmail());
        if (isAdd){
            user.update("fav", FieldValue.arrayUnion(anime));
        }else {
            user.update("fav", FieldValue.arrayRemove(anime));
        }
    }

    public void updateFriends(DocumentSnapshot friend, boolean isAdd) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference user = db.collection("users").document(Controller.usuario.getEmail());
        if (isAdd){
            user.update("friends", FieldValue.arrayUnion(friend));
        }else {
            user.update("friends", FieldValue.arrayRemove(friend));
        }
    }

    public Task<QuerySnapshot> getAllUsers() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        return db.collection("users").get();
    }
}
