package com.example.animefinal.controller;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.animefinal.R;
import com.example.animefinal.dao.FirestoreDB;
import com.example.animefinal.model.Anime;
import com.example.animefinal.model.Usuario;
import com.example.animefinal.utils.AnimeAdapter;
import com.example.animefinal.utils.FriendsProfileAdapter;
import com.example.animefinal.view.LogActivity;
import com.example.animefinal.view.MenuActivity;
import com.example.animefinal.view.ProfileActivity;
import com.example.animefinal.view.SearchActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

public class SearchController implements ControllerInterface {
    private SearchActivity searchActivity;
    private ProfileActivity profileActivity;
    private LogActivity loginActivity;
    private MenuActivity menuActivity;
    private FirestoreDB persistencia;
    private static String JSON_URL = "https://joanseculi.com/edt69/animes2.php?email=admin2@email.com";
    private ArrayList<Anime> animes = new ArrayList<>();
    private ArrayList<Usuario> users = new ArrayList<>();
    private RecyclerView recyclerViewSearch;
    EditText input;
    String inputStr;
    Button searchBtn, searchUser;


    public SearchController(SearchActivity searchActivity, FirestoreDB persistencia) {
        this.searchActivity = searchActivity;
        this.persistencia = persistencia;
    }

    public void start(){
        searchUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                persistencia.getAllUsers().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        users.clear();
                        inputStr = input.getText().toString().toLowerCase() + "";

                        for (QueryDocumentSnapshot document : task.getResult()) {
                            if (document.get("name").toString().toLowerCase().contains(inputStr) && !document.get("email").equals(Controller.usuario.getEmail())){
                                Usuario user = new Usuario((String) document.get("name"),(String) document.get("email"),(String) document.get("username"));
                                users.add(user);
                            }
                        }

                        FriendsProfileAdapter friendsProfileAdapter = new FriendsProfileAdapter(users, searchActivity.getApplicationContext(),persistencia);
                        recyclerViewSearch.setAdapter(friendsProfileAdapter);
                        recyclerViewSearch.setLayoutManager(new LinearLayoutManager(searchActivity.getApplicationContext()));
                    }
                });
            }
        });

        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animes.clear();
                inputStr = input.getText().toString().toLowerCase() + "";

                RequestQueue queue = Volley.newRequestQueue(searchActivity);
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                        Request.Method.GET,
                        JSON_URL,
                        null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    JSONArray jsonArray = response.getJSONArray("animes");
                                    for (int i = 0 ; i < jsonArray.length() ; i++) {
                                        //animes.clear();
                                        JSONObject animeObject = jsonArray.getJSONObject(i);
                                        if (animeObject.getString("name").toLowerCase().contains(inputStr)){
                                            Anime anime = new Anime();
                                            anime.setName(animeObject.getString("name"));
                                            anime.setDescription(animeObject.getString("description"));
                                            anime.setType(animeObject.getString("type"));
                                            anime.setYear(animeObject.getInt("year"));
                                            anime.setImage(animeObject.getString("image"));
                                            animes.add(anime);
                                        }
                                    }

                                    AnimeAdapter animeAdapter = new AnimeAdapter(animes, searchActivity.getApplicationContext(),persistencia);
                                    recyclerViewSearch.setAdapter(animeAdapter);
                                    recyclerViewSearch.setLayoutManager(new LinearLayoutManager(searchActivity.getApplicationContext()));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d("tag", "onErrorResponse: " + error.getMessage());
                            }
                        }
                );
                queue.add(jsonObjectRequest);
            }
        });
    }


    @Override
    public void createActivityButtons() {
        recyclerViewSearch = searchActivity.getRecyclerView();
        input = searchActivity.getInput();
        searchBtn = searchActivity.getSearchBtn();
        searchUser = searchActivity.getSearchUser();
    }

    public void toolbarStart() {
        this.loginActivity = new LogActivity();
        this.menuActivity = new MenuActivity();
        this.profileActivity = new ProfileActivity();

        searchActivity.getHome().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchActivity(searchActivity,menuActivity);
            }
        });
        searchActivity.getProfile().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchActivity(searchActivity,profileActivity);
            }
        });
        searchActivity.getSearch().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchActivity(searchActivity,searchActivity);
            }
        });
        searchActivity.getLogout().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearSession(searchActivity,loginActivity);
            }
        });
    }
}
