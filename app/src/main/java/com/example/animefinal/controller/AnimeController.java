package com.example.animefinal.controller;

import android.os.Build;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.animefinal.R;
import com.example.animefinal.dao.FirestoreDB;
import com.example.animefinal.model.Anime;
import com.example.animefinal.utils.FavProfileAdapter;
import com.example.animefinal.view.AnimeActivity;
import com.example.animefinal.view.LogActivity;
import com.example.animefinal.view.MenuActivity;
import com.example.animefinal.view.ProfileActivity;
import com.example.animefinal.view.ProfileFriendActivity;
import com.example.animefinal.view.SearchActivity;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Map;

public class AnimeController implements ControllerInterface {
    private AnimeActivity animeActivity;
    private LogActivity loginActivity;
    private ProfileActivity profileActivity;
    private SearchActivity searchActivity;
    private MenuActivity menuActivity;
    private FirestoreDB persistencia;
    private FirebaseFirestore db;
    TextView name,type,year,desc;
    ImageView img;
    String nameStr, typeStr, yearStr,descStr, imageStr;
    public AnimeController(AnimeActivity animeActivity, FirestoreDB persistencia) {
        this.animeActivity = animeActivity;
        this.persistencia = persistencia;
        this.db = FirebaseFirestore.getInstance();

    }

    public void start(){
        name.setText(nameStr);
        type.setText(typeStr);
        year.setText(yearStr);
        desc.setText(descStr);
        Picasso.get().load("https://joanseculi.com/" + imageStr).into(img);
    }

    @Override
    public void createActivityButtons() {
        name = animeActivity.getName();
        type = animeActivity.getType();
        year = animeActivity.getYear();
        desc = animeActivity.getDesc();
        img = animeActivity.getImg();
        nameStr = animeActivity.getNameStr();
        typeStr = animeActivity.getTypeStr();
        descStr = animeActivity.getDescStr();
        yearStr = animeActivity.getYearStr();
        imageStr = animeActivity.getImageStr();
    }

    public void toolbarStart() {
        this.loginActivity = new LogActivity();
        this.searchActivity = new SearchActivity();
        this.profileActivity = new ProfileActivity();
        this.menuActivity = new MenuActivity();

        animeActivity.getHome().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchActivity(animeActivity,menuActivity);
            }
        });
        animeActivity.getProfile().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchActivity(animeActivity,profileActivity);
            }
        });
        animeActivity.getSearch().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchActivity(animeActivity,searchActivity);
            }
        });
        animeActivity.getLogout().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearSession(animeActivity,loginActivity);
            }
        });
    }
}
