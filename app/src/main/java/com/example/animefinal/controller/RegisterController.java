package com.example.animefinal.controller;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.animefinal.dao.FirestoreDB;
import com.example.animefinal.model.Usuario;
import com.example.animefinal.view.LogActivity;
import com.example.animefinal.view.MenuActivity;
import com.example.animefinal.view.RegisterActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class RegisterController implements ControllerInterface{
    private RegisterActivity registerActivity;
    private FirestoreDB persistencia;
    private Button registerButton;
    private TextView loginRed;
    private MenuActivity menuActivity;
    private LogActivity logActivity;

    public RegisterController(RegisterActivity registerActivity, FirestoreDB persistencia) {
        this.registerActivity = registerActivity;
        this.persistencia = persistencia;
        menuActivity = new MenuActivity();
        logActivity = new LogActivity();
    }

    public void start(){
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = registerActivity.getName().getText().toString();
                String email = registerActivity.getEmail().getText().toString();
                String password = registerActivity.getPassword().getText().toString();
                String username = registerActivity.getUsername().getText().toString();

                if (!email.isEmpty() && !password.isEmpty()) {
                    FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                saveSesion(registerActivity, email);
                                Controller.usuario = new Usuario(name, email, username);
                                persistencia.save();
                                switchActivity(registerActivity,menuActivity);
                            } else {
                                showAlert(registerActivity, "Formato del email incorrecto o contrasenya poco compleja");
                            }
                        }
                    });
                } else {
                    showAlert(registerActivity, "Error en el registro");
                }
            }
        });

        loginRed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchActivity(registerActivity,logActivity);
            }
        });

    }

    @Override
    public void createActivityButtons() {
        registerButton = registerActivity.getBtn();
        loginRed = registerActivity.getLoginRed();
    }
}
