package com.example.animefinal.controller;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.animefinal.model.Usuario;
import com.example.animefinal.view.LogActivity;
import com.example.animefinal.view.MenuActivity;
import com.example.animefinal.view.RegisterActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LogController implements ControllerInterface{
    private LogActivity logActivity;
    private MenuActivity menuActivity;
    private RegisterActivity signupActivity;
    private TextView signupRed;
    private Button loginBtn;

    public LogController(LogActivity logActivity) {
        this.logActivity = logActivity;
        this.menuActivity = new MenuActivity();
        this.signupActivity = new RegisterActivity();
    }

    public void start(){
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = logActivity.getEmail().getText().toString();
                String password = logActivity.getPassword().getText().toString();

                if (!email.isEmpty() && !password.isEmpty()) {
                    FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                saveSesion(logActivity, email);
                                Controller.usuario = new Usuario(email,password);
                                switchActivity(logActivity, menuActivity, "user", Controller.usuario);
                            } else {
                                showAlert(logActivity, "Usuario o Contrasenya incorrecta");
                            }
                        }
                    });
                } else {
                    showAlert(logActivity, "Error en el login");
                }
            }
        });

        signupRed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchActivity(logActivity,signupActivity);
            }
        });
    }

    @Override
    public void createActivityButtons() {
        signupRed = logActivity.getSignupRed();
        loginBtn = logActivity.getLoginBtn();
    }

}
