package com.example.animefinal.controller;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.animefinal.R;
import com.example.animefinal.dao.FirestoreDB;
import com.example.animefinal.model.Anime;
import com.example.animefinal.model.Usuario;
import com.example.animefinal.utils.AnimeAdapter;
import com.example.animefinal.utils.FavProfileAdapter;
import com.example.animefinal.utils.FriendsProfileAdapter;
import com.example.animefinal.view.LogActivity;
import com.example.animefinal.view.MenuActivity;
import com.example.animefinal.view.ProfileActivity;
import com.example.animefinal.view.SearchActivity;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ProfileController implements ControllerInterface {
    private ProfileActivity profileActivity;
    private LogActivity loginActivity;
    private SearchActivity searchActivity;
    private MenuActivity menuActivity;
    private FirestoreDB persistencia;
    private FirebaseFirestore db;
    TextView name, username;
    private ArrayList<Anime> animes = new ArrayList<>();
    private ArrayList<Usuario> friends = new ArrayList<>();
    private RecyclerView recyclerViewFav, recyclerViewFriends;
    private Button follow;


    public ProfileController(ProfileActivity profileActivity, FirestoreDB persistencia) {
        this.profileActivity = profileActivity;
        this.menuActivity = new MenuActivity();
        this.persistencia = persistencia;
        this.db = FirebaseFirestore.getInstance();
    }

    public void start(){
        persistencia.getUser().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                name.setText(documentSnapshot.get("name").toString());
                username.setText(documentSnapshot.get("username").toString());

                for (Map<String, Object> map : (ArrayList<Map<String, Object>>) documentSnapshot.get("fav")) {
                    Anime anime = new Anime();
                    anime.setName((String) map.get("name"));
                    anime.setDescription((String) map.get("description"));
                    anime.setType((String) map.get("type"));
                    Long year = (Long) map.get("year");
                    anime.setYear((Integer) year.intValue());
                    anime.setImage((String) map.get("image"));
                    animes.add(anime);
                }

                FavProfileAdapter favProfileAdapter = new FavProfileAdapter(animes, profileActivity.getApplicationContext(),persistencia,false);
                recyclerViewFav.setAdapter(favProfileAdapter);
                recyclerViewFav.setLayoutManager(new LinearLayoutManager(profileActivity.getApplicationContext()));

               for (Map<String, Object> map : (ArrayList<Map<String, Object>>) documentSnapshot.get("friends")) {
                   Map<String, Object> data = (Map<String, Object>) map.get("data");
                   Usuario usuario = new Usuario((String) data.get("name"), (String) data.get("email"), (String) data.get("username"));
                   friends.add(usuario);
                }
                FriendsProfileAdapter friendsProfileAdapter = new FriendsProfileAdapter(friends, profileActivity.getApplicationContext(),persistencia);
                recyclerViewFriends.setAdapter(friendsProfileAdapter);
                recyclerViewFriends.setLayoutManager(new LinearLayoutManager(profileActivity.getApplicationContext()));
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                switchActivity(profileActivity, menuActivity);
            }
        });

    }

    @Override
    public void createActivityButtons() {
        name = profileActivity.getName();
        username = profileActivity.getUsername();
        recyclerViewFav = profileActivity.getRecyclerViewFavs();
        recyclerViewFriends = profileActivity.getRecyclerViewFriends();
        follow = profileActivity.getFollow();
    }

    public void toolbarStart() {
        this.loginActivity = new LogActivity();
        this.searchActivity = new SearchActivity();

        profileActivity.getHome().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchActivity(profileActivity,menuActivity);
            }
        });
        profileActivity.getProfile().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchActivity(profileActivity,profileActivity);
            }
        });
        profileActivity.getSearch().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchActivity(profileActivity,searchActivity);
            }
        });
        profileActivity.getLogout().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearSession(profileActivity,loginActivity);
            }
        });
    }
}
