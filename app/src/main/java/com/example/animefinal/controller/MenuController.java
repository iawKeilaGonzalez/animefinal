package com.example.animefinal.controller;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.animefinal.dao.FirestoreDB;
import com.example.animefinal.model.Anime;
import com.example.animefinal.utils.AnimeAdapter;
import com.example.animefinal.view.LogActivity;
import com.example.animefinal.view.MenuActivity;
import com.example.animefinal.view.ProfileActivity;
import com.example.animefinal.view.SearchActivity;
import com.google.firebase.firestore.FirebaseFirestore;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MenuController implements ControllerInterface {
    private MenuActivity menuActivity;
    private LogActivity loginActivity;
    private ProfileActivity profileActivity;
    private SearchActivity searchActivity;
    private FirestoreDB persistencia;
    private FirebaseFirestore db;
    private ProgressBar progressBar;
    private static String JSON_URL = "https://joanseculi.com/edt69/animes2.php?email=admin2@email.com";
    private ArrayList<Anime> animes = new ArrayList<>();
    private RecyclerView recyclerViewAnimes;

    public MenuController(MenuActivity menuActivity, FirestoreDB persistencia) {
        this.menuActivity = menuActivity;
        this.persistencia = persistencia;
        this.db = FirebaseFirestore.getInstance();
    }

    @Override
    public void createActivityButtons() {
        recyclerViewAnimes = menuActivity.getRecyclerView();
    }

    public void toolbarStart() {
        this.loginActivity = new LogActivity();
        this.profileActivity = new ProfileActivity();
        this.searchActivity = new SearchActivity();

        menuActivity.getHome().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchActivity(menuActivity,menuActivity);
            }
        });
        menuActivity.getProfile().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchActivity(menuActivity,profileActivity);
            }
        });
        menuActivity.getSearch().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchActivity(menuActivity,searchActivity);
            }
        });
        menuActivity.getLogout().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearSession(menuActivity,loginActivity);
            }
        });
    }

    public void start() {
        RequestQueue queue = Volley.newRequestQueue(menuActivity);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                JSON_URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("animes");
                            for (int i = 0 ; i < jsonArray.length() ; i++) {
                                JSONObject animeObject = jsonArray.getJSONObject(i);
                                Anime anime = new Anime();
                                anime.setName(animeObject.getString("name"));
                                anime.setDescription(animeObject.getString("description"));
                                anime.setType(animeObject.getString("type"));
                                anime.setYear(animeObject.getInt("year"));
                                anime.setImage(animeObject.getString("image"));
                                animes.add(anime);

                            }
                            AnimeAdapter animeAdapter = new AnimeAdapter(animes, menuActivity.getApplicationContext(),persistencia);
                            recyclerViewAnimes.setAdapter(animeAdapter);
                            recyclerViewAnimes.setLayoutManager(new LinearLayoutManager(menuActivity.getApplicationContext()));


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("tag", "onErrorResponse: " + error.getMessage());
                    }
                }
        );
        queue.add(jsonObjectRequest);
    }
}

