package com.example.animefinal.controller;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.annotation.NonNull;

import com.example.animefinal.R;
import com.example.animefinal.dao.FirestoreDB;
import com.example.animefinal.model.Usuario;
import com.example.animefinal.view.AnimeActivity;
import com.example.animefinal.view.LogActivity;
import com.example.animefinal.view.MenuActivity;
import com.example.animefinal.view.ProfileActivity;
import com.example.animefinal.view.ProfileFriendActivity;
import com.example.animefinal.view.RegisterActivity;
import com.example.animefinal.view.SearchActivity;

public class Controller implements ControllerInterface{
    private RegisterActivity registerActivity;
    private RegisterController registerController;
    private LogActivity logActivity;
    private LogController logController;
    private MenuActivity menuActivity;
    private MenuController menuController;
    private SearchActivity searchActivity;
    private SearchController searchController;
    private AnimeActivity animeActivity;
    private AnimeController animeController;
    private ProfileActivity profileActivity;
    private ProfileController profileController;
    private ProfileFriendActivity profileFriendActivity;
    private ProfileFriendController profileFriendController;
    private FirestoreDB persistencia;
    private static Controller controller;
    public static Usuario usuario;

    public static Controller getInstance() {
        if (controller == null) controller = new Controller();
        return controller;
    }

    private Controller () {
        this.registerActivity = new RegisterActivity();
        this.logActivity = new LogActivity();
        this.persistencia = new FirestoreDB();
        this.profileActivity= new ProfileActivity();
    }

    public void registerActivity(RegisterActivity registerActivity) {
        this.registerActivity = registerActivity;
        this.registerActivity.createAllItemsAsGlobalWithGetters();
        this.registerController = new RegisterController(this.registerActivity,this.persistencia);
        this.registerController.createActivityButtons();
        this.registerController.start();
    }

    public void logActivity(LogActivity logActivity){
        this.logActivity = logActivity;
        this.logActivity.createAllItemsAsGlobalWithGetters();
        this.logController = new LogController(this.logActivity);
        this.logController.createActivityButtons();
        this.logController.start();
    }

    public void profileActivity (ProfileActivity profileActivity){
        this.profileActivity = profileActivity;
        this.profileActivity.createAllItemsAsGlobalWithGetters();
        this.profileController = new ProfileController(this.profileActivity, this.persistencia);
        this.profileController.createActivityButtons();
        this.profileController.start();
        this.profileController.toolbarStart();
    }

    public void animeActivity (AnimeActivity animeActivity){
        this.animeActivity = animeActivity;
        this.animeActivity.createAllItemsAsGlobalWithGetters();
        this.animeController = new AnimeController(this.animeActivity, this.persistencia);
        this.animeController.createActivityButtons();
        this.animeController.start();
        this.animeController.toolbarStart();
    }

    public void profileFriendActivity (ProfileFriendActivity profileFriendActivity){
        this.profileFriendActivity = profileFriendActivity;
        this.profileFriendActivity.createAllItemsAsGlobalWithGetters();
        this.profileFriendController = new ProfileFriendController(this.profileFriendActivity, this.persistencia);
        this.profileFriendController.createActivityButtons();
        this.profileFriendController.start();
        this.profileFriendController.toolbarStart();
    }

    public void menuActivity(MenuActivity menuActivity) {
        this.menuActivity = menuActivity;
        this.menuActivity.createAllItemsAsGlobalWithGetters();
        this.menuController = new MenuController(this.menuActivity, this.persistencia);
        this.menuController.createActivityButtons();
        this.menuController.start();
        this.menuController.toolbarStart();

    }

    public void searchActivity(SearchActivity searchActivity) {
        this.searchActivity = searchActivity;
        this.searchActivity.createAllItemsAsGlobalWithGetters();
        this.searchController = new SearchController(this.searchActivity, this.persistencia);
        this.searchController.createActivityButtons();
        this.searchController.start();
        this.searchController.toolbarStart();

    }

    @Override
    public void createActivityButtons() {
    }
}
