package com.example.animefinal.controller;

import android.os.Build;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.animefinal.R;
import com.example.animefinal.dao.FirestoreDB;
import com.example.animefinal.model.Anime;
import com.example.animefinal.utils.FavProfileAdapter;
import com.example.animefinal.view.LogActivity;
import com.example.animefinal.view.MenuActivity;
import com.example.animefinal.view.ProfileActivity;
import com.example.animefinal.view.ProfileFriendActivity;
import com.example.animefinal.view.SearchActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProfileFriendController implements ControllerInterface {
    private ProfileFriendActivity profileFriendActivity;
    private LogActivity loginActivity;
    private ProfileActivity profileActivity;
    private SearchActivity searchActivity;
    private MenuActivity menuActivity;
    private FirestoreDB persistencia;
    private FirebaseFirestore db;
    TextView name, username;
    private ArrayList<Anime> animes = new ArrayList<>();
    private RecyclerView recyclerViewFavFriend;
    String nameStr, usernameStr, emailStr;
    Button follow;

    public ProfileFriendController(ProfileFriendActivity profileFriendActivity, FirestoreDB persistencia) {
        this.profileFriendActivity = profileFriendActivity;
        this.menuActivity = new MenuActivity();
        this.persistencia = persistencia;
        this.db = FirebaseFirestore.getInstance();

    }

    public void start(){
        persistencia.getFriend(emailStr).addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                name.setText(nameStr);
                username.setText(usernameStr);

                for (Map<String, Object> map : (ArrayList<Map<String, Object>>) documentSnapshot.get("fav")) {
                    Anime anime = new Anime();
                    anime.setName((String) map.get("name"));
                    anime.setDescription((String) map.get("description"));
                    anime.setType((String) map.get("type"));
                    Long year = (Long) map.get("year");
                    anime.setYear((Integer) year.intValue());
                    anime.setImage((String) map.get("image"));
                    animes.add(anime);
                }

                FavProfileAdapter favProfileAdapter = new FavProfileAdapter(animes, profileFriendActivity.getApplicationContext(),persistencia,true);
                recyclerViewFavFriend.setAdapter(favProfileAdapter);
                recyclerViewFavFriend.setLayoutManager(new LinearLayoutManager(profileFriendActivity.getApplicationContext()));


                persistencia.getUser().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        boolean existe = false;
                        for (Map<String, Object> map : (ArrayList<Map<String, Object>>) documentSnapshot.get("friends")) {
                            Map<String, Object> data = (Map<String, Object>) map.get("data");
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    if (data.get("email").equals(emailStr)){
                                        existe = true;
                                        break;
                                    }
                                }
                        }
                        if (existe){
                            follow.setText("UNFOLLOW");
                            follow.setBackgroundColor(profileFriendActivity.getResources().getColor(R.color.grey));
                        }else{
                            follow.setText("FOLLOW");
                            follow.setBackgroundColor(profileFriendActivity.getResources().getColor(R.color.purpleDark));
                        }
                    }
                });

                follow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(follow.getText().toString().toUpperCase().equals("FOLLOW")) {
                            follow.setText("UNFOLLOW");

                            persistencia.getFriend(emailStr).addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                @Override
                                public void onSuccess(DocumentSnapshot documentSnapshot) {
                                    persistencia.updateFriends(documentSnapshot,true);
                                }
                            });
                        } else {
                            follow.setText("FOLLOW");
                            persistencia.getFriend(emailStr).addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                @Override
                                public void onSuccess(DocumentSnapshot documentSnapshot) {
                                    persistencia.updateFriends(documentSnapshot,false);
                                }
                            });
                        }
                    }
                });
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                switchActivity(profileActivity, menuActivity);
            }
        });

    }

    @Override
    public void createActivityButtons() {
        name = profileFriendActivity.getName();
        username = profileFriendActivity.getUsername();
        nameStr = profileFriendActivity.getNameStr();
        usernameStr = profileFriendActivity.getUsernameStr();
        emailStr = profileFriendActivity.getEmailStr();
        recyclerViewFavFriend = profileFriendActivity.getRecyclerViewFavsFriend();
        follow = profileFriendActivity.getFollow();
    }

    public void toolbarStart() {
        this.loginActivity = new LogActivity();
        this.searchActivity = new SearchActivity();
        this.profileActivity = new ProfileActivity();

        profileFriendActivity.getHome().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchActivity(profileFriendActivity,menuActivity);
            }
        });
        profileFriendActivity.getProfile().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchActivity(profileFriendActivity,profileActivity);
            }
        });
        profileFriendActivity.getSearch().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchActivity(profileFriendActivity,searchActivity);
            }
        });
        profileFriendActivity.getLogout().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearSession(profileFriendActivity,loginActivity);
            }
        });
    }
}
