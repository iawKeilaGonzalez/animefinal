package com.example.animefinal.controller;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;

import com.example.animefinal.R;
import com.example.animefinal.model.Usuario;
import com.example.animefinal.utils.Constants;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.firebase.Timestamp;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;


public interface ControllerInterface {

    String pattern = "dd/MM/yyyy";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

    void createActivityButtons();

    default String ifNullString(Object o) {
        if (o == null) return "";
        if (o instanceof Timestamp) return ((Timestamp) o).toDate().toString();
        return (String) o;
    }

    default int ifNullInt(Object o) {
        if (o == null) return 0;
        return (int) ((long) o);
    }

    default void showAlert(Activity activity) {
        showAlert(activity,"Se ha porducido un error");
    }

    default void showAlert(Activity activity, String message) {
        showAlert(activity, message, "ERROR");
    }

    default void showAlert(Activity activity, String message, String title) {
        new MaterialAlertDialogBuilder(activity, R.style.RoundShapeTheme)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("Aceptar", null)
                .create().show();
    }


    default void switchActivity(Activity from, Activity to) {
        switchActivity(from, to, "", null);
    }

    default void switchActivity(Activity from, Activity to, String extraKey, Object extra) {
        Intent intent = new Intent(from, to.getClass());
        if (extra != null) {
            intent.putExtra(extraKey, (Serializable) extra);
        }
        from.startActivity(intent);
    }

    default void saveSesion(Activity activity, String email) {
        SharedPreferences data = activity.getSharedPreferences(Constants.PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = data.edit();
        editor.putString("email", email);
        editor.apply();
    }

    default String readDataWithSharePrefence(Activity activity) {
        SharedPreferences data = activity.getSharedPreferences(Constants.PREFERENCES, Context.MODE_PRIVATE);
        if(data.getString("email", null) == null) {
            return null;
        } else {
            Controller.usuario = new Usuario(data.getString("email", null), todayDate());
            return Controller.usuario.getEmail();
        }
    }

    default void clearSession (Activity activity, Activity desti) {
        // Borrar datos de sesión
        SharedPreferences.Editor prefs = activity.getSharedPreferences(
                Constants.PREFERENCES, Context.MODE_PRIVATE).edit();
        prefs.clear();
        prefs.apply();
        switchActivity(activity, desti);
    }

    default int dpToPx(int dp, Activity activity) {
        return (int) (dp * activity.getResources().getDisplayMetrics().density + 0.5f);
    }

    default String todayDate() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar
                .getInstance().getTime());
    }

}